import React, {Fragment} from 'react';
import {Button, Form, FormGroup, Label, Input, CustomInput, Card, CardTitle, CardText, CardImg, Row, Col} from 'reactstrap';
import {getQuiz, getSession} from '../utils/getQuiz';
import isEmpty from "lodash/isEmpty";

import QuestionCard from "../containers/QuestionCard";


class App extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            number: '10',
            category: '10',
            difficulty: 'easy',
            sessionToken: null,
            quizData: null,
        };

        this.chooseNumberQuestions = this.chooseNumberQuestions.bind(this);
        this.chooseCategory = this.chooseCategory.bind(this);
        this.chooseDifficulty = this.chooseDifficulty.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        getSession().then(session =>
            this.setState({sessionToken: session.token})
        );
    }

    chooseNumberQuestions(event) {
        this.setState({number: event.target.value});
    }

    chooseCategory(event) {
        this.setState({category: event.target.value});
    }

    chooseDifficulty(event) {
        this.setState({difficulty: event.target.value});
    }


    fetchCategory(number, category, difficulty, sessionToken) {
        return () => {
            const {setQuizData} = this.props;
            getQuiz(number, category, difficulty, sessionToken).then(quizData =>
                setQuizData(quizData.results)
            );
        };
    }

    handleSubmit(event, fetchCategory) {
        const {setQuizData} = this.props;
        getQuiz(this.state.number, this.state.category, this.state.difficulty, this.state.sessionToken).then(quizData =>
            setQuizData(quizData.results));
        event.preventDefault();

    }

    restartGame() {
        this.props.setQuizData(null);
        return this.setState({
            quizData: null,
            rightAnswers: 0,
            currentQuestion: 0,
            categorySelected: false
        });
    }

    updateData = (value) => {
        this.setState({ hasPressButton: value })
    }


    render() {
        const {categories, quizData} = this.props;
        const {
            sessionToken,
            rightAnswers,
            currentQuestion,
            categorySelected,
            hasPressButton
        } = this.state;
        let result = 0;


        return (

            <div className="test-app">
                <header className="test-app__header">
                    <div className='container'>
                        <div className='row'>
                            <div className='col-md-12'>
                                <h1>Welcome to Testing App</h1>
                            </div>
                        </div>
                    </div>
                </header>
                <main className="test-app__content">
                    {(isEmpty(quizData) && !categorySelected) &&
                    <div className='container'>
                        <div className='row'>
                            <div className='col-md-12'>
                                <h2>Fill in the form, please.</h2>
                                <Form className='start-form' onSubmit={this.handleSubmit}>
                                    <FormGroup>
                                        <Label for="startFormName">Your Name:</Label>
                                        <Input type="text" name="name" id="startFormName"
                                               placeholder="please enter your name"/>
                                    </FormGroup>
                                    <FormGroup>
                                        <Label for="startFormCountQuestions">Number of questions:</Label>
                                        <Input type="number"
                                               name="countQuestions"
                                               id="startFormCountQuestions"
                                               placeholder="please press count questions for test"
                                               value={this.state.number}
                                               onChange={this.chooseNumberQuestions}
                                        />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label for="startFormSelectCategory">Select Category:</Label>
                                        <Input
                                            type="select"
                                            name="selectCategory"
                                            id="startFormSelectCategory"
                                            value={this.state.category}
                                            onChange={this.chooseCategory}>
                                            <option value="9">General Knowledge</option>
                                            <option value="10">Entertainment: Books</option>
                                            <option value="11">Entertainment: Film</option>
                                            <option value="12">Entertainment: Music</option>
                                            <option value="13">Entertainment: Musicals &amp; Theatres</option>
                                            <option value="14">Entertainment: Television</option>
                                            <option value="15">Entertainment: Video Games</option>
                                            <option value="16">Entertainment: Board Games</option>
                                            <option value="17">Science &amp; Nature</option>
                                            <option value="18">Science: Computers</option>
                                            <option value="19">Science: Mathematics</option>
                                            <option value="20">Mythology</option>
                                            <option value="21">Sports</option>
                                            <option value="22">Geography</option>
                                            <option value="23">History</option>
                                            <option value="24">Politics</option>
                                            <option value="25">Art</option>
                                            <option value="26">Celebrities</option>
                                            <option value="27">Animals</option>
                                            <option value="28">Vehicles</option>
                                            <option value="29">Entertainment: Comics</option>
                                            <option value="30">Science: Gadgets</option>
                                            <option value="31">Entertainment: Japanese Anime &amp; Manga</option>
                                            <option value="32">Entertainment: Cartoon &amp; Animations</option>
                                        </Input>
                                    </FormGroup>
                                    <FormGroup>
                                        <Label for="startFormSelectDifficulty">Select Difficulty:</Label>
                                        <Input
                                            type="select"
                                            name="selectDifficulty"
                                            id="startFormSelectDifficulty"
                                            value={this.state.difficulty}
                                            onChange={this.chooseDifficulty}>
                                            <option value="easy">Easy</option>
                                            <option value="medium">Medium</option>
                                            <option value="hard">Hard</option>
                                        </Input>
                                    </FormGroup>
                                    <Button>Submit</Button>
                                </Form>
                            </div>
                        </div>
                    </div>
                    }
                    {!(isEmpty(quizData))
                        ? <div className='container'>
                            <div className='row'>
                                <div className='col-md-12'>
                                    {quizData.map((item, i) => {
                                        return(
                                            <Fragment key={i}>
                                            <QuestionCard
                                                question = {item.question}
                                                ansverCorect = {item.correct_answer}
                                                ansverIncorrect = {item.incorrect_answers}
                                                index = {i}
                                                updateData={this.updateData}
                                            />
                                            </Fragment>
                                            )
                                    })}

                                    <div className='result'>
                                        OUR RESULT: {result}/{this.state.number}
                                    </div>
                                </div>
                            </div>
                        </div>
                        : ''
                    }
                </main>
            </div>

        );
    }
}

export default App;