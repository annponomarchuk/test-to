import React from 'react';
import ReactDOM from 'react-dom';
import './app/scss/index.scss';
import App from './app/App';
import 'bootstrap/dist/css/bootstrap.min.css';
import registerServiceWorker from './app/registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
