import React, {Component} from 'react';

import StartForm from '../containers/StartForm';
import DisplayTest from '../containers/DisplayTest';
import { connect } from 'react-redux';
import { setQuizQuestions } from '../actions/actions';
import {store} from '../store/index';



class App extends Component {
    render() {
        return (
            <DisplayTest store={store}  />
        );
    }
}

export default App;
