import React from 'react';
import App from '../../components/app';
import { connect } from 'react-redux';
import { setQuizQuestions } from '../../actions/actions';
import {getQuiz} from "../../utils/getQuiz";

const mapStateToProps = state => {
    return {
        quizData: state.quizData
    };
};

const mapDispatchToProps = dispatch => {
    return {
        setQuizData: quizData => dispatch(setQuizQuestions(quizData))
    };
};

const DisplayTest = connect(
    mapStateToProps,
    mapDispatchToProps
)(App);

export default DisplayTest;