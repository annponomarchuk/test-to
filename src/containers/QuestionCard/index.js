import React, {Fragment, PureComponent} from "react";
import {Button, Form, FormGroup, Label, CustomInput} from 'reactstrap';

class QuestionCard extends PureComponent {
    constructor(props) {

        super(props);
        this.state = {
            chooseAnsver: '',
            returnResult: true,
            correctAnswer: atob(this.props.ansverCorect),
            hasPressButton: false,
        };

        this.chooseCorrectAnsver = this.chooseCorrectAnsver.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    chooseCorrectAnsver(event) {
        this.setState({chooseAnsver: event.target.value});
    }

    checkAnswer(answer, correctAnswer) {
        return () => {
            if ((answer) === (correctAnswer)) {
                this.setState({returnResult: true});
            } else {
                this.setState({returnResult: false});
            }
            this.setState({hasPressButton: true});
        };

    }

    handleSubmit(event) {
        this.checkAnswer(this.chooseAnsver, this.correctAnswer);
        event.preventDefault();

    }

    render() {
        const {
            question,
            ansverCorect,
            ansverIncorrect,
            index,
            result,
        } = this.props;
        const ansvers = ansverIncorrect.concat(ansverCorect).sort();
        const checkAnswer = this.state.returnResult;

        return (
            <Form>
                <FormGroup>
                    <Label for={`question${index + 1}`}>{index + 1}. {atob(question)}</Label>

                    <div>
                        {ansvers.map((ansver, k) => {
                            return (
                                <Fragment key={k}>
                                    <CustomInput type="radio" id={`question${index + 1}${k}`}
                                                 name="customRadio" label={atob(ansver)}
                                                 value={`${atob(ansver)}`}
                                                 onChange={this.chooseCorrectAnsver}
                                                 disabled={this.state.hasPressButton}
                                    />
                                </Fragment>
                            )
                        })
                        }
                    </div>
                </FormGroup>
                <Button type="button" className={`${checkAnswer && this.state.hasPressButton  && 'btn-success'} ${!checkAnswer && this.state.hasPressButton  && 'btn-danger'}`} onClick={this.checkAnswer(this.state.chooseAnsver, this.state.correctAnswer)} disabled={this.state.hasPressButton}>Ответить</Button>
            </Form>
        );
    };
}



export default QuestionCard;
