import React, {PureComponent} from "react";
import {Button, Form, FormGroup, Label, Input} from 'reactstrap';
import {getQuiz} from '../../utils/getQuiz';
import {setQuizQuestions} from "../../actions/actions";
import {connect} from "react-redux";
import App from "../../app/App";

class StartForm extends PureComponent {
    constructor(props) {

        super(props);
        this.state = {
            number: '10',
            category: '10',
            difficulty: 'easy'
        };

        this.chooseNumberQuestions = this.chooseNumberQuestions.bind(this);
        this.chooseCategory = this.chooseCategory.bind(this);
        this.chooseDifficulty = this.chooseDifficulty.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    chooseNumberQuestions(event) {
        this.setState({number: event.target.value});
    }
    chooseCategory(event) {
        this.setState({category: event.target.value});
    }

    chooseDifficulty(event) {
        this.setState({difficulty: event.target.value});
    }

    fetchCategory(number, category, difficulty) {
        return () => {
            const { setQuizData } = this.props;
            getQuiz(number, category, difficulty).then(quizData =>
                setQuizData(quizData.results)
            );
        };
    }

    handleSubmit(event) {
        getQuiz(this.state.number, this.state.category, this.state.difficulty);
        //this.fetchCategory(this.state.number, this.state.category, this.state.difficulty);
        event.preventDefault();

    }

    render() {
        return (
            <Form className='start-form' onSubmit={this.handleSubmit}>
                <FormGroup>
                    <Label for="startFormName">Your Name:</Label>
                    <Input type="text" name="name" id="startFormName" placeholder="please enter your name"/>
                </FormGroup>
                <FormGroup>
                    <Label for="startFormCountQuestions">Number of questions:</Label>
                    <Input type="number"
                           name="countQuestions"
                           id="startFormCountQuestions"
                           placeholder="please press count questions for test"
                           value={this.state.number}
                           onChange={this.chooseNumberQuestions}
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="startFormSelectCategory">Select Category:</Label>
                    <Input
                        type="select"
                        name="selectCategory"
                        id="startFormSelectCategory"
                        value={this.state.category}
                        onChange={this.chooseCategory}>
                        <option value="9">General Knowledge</option>
                        <option value="10">Entertainment: Books</option>
                        <option value="11">Entertainment: Film</option>
                        <option value="12">Entertainment: Music</option>
                        <option value="13">Entertainment: Musicals &amp; Theatres</option>
                        <option value="14">Entertainment: Television</option>
                        <option value="15">Entertainment: Video Games</option>
                        <option value="16">Entertainment: Board Games</option>
                        <option value="17">Science & Nature</option>
                        <option value="18">Science: Computers</option>
                        <option value="19">Science: Mathematics</option>
                        <option value="20">Mythology</option>
                        <option value="21">Sports</option>
                        <option value="22">Geography</option>
                        <option value="23">History</option>
                        <option value="24">Politics</option>
                        <option value="25">Art</option>
                        <option value="26">Celebrities</option>
                        <option value="27">Animals</option>
                        <option value="28">Vehicles</option>
                        <option value="29">Entertainment: Comics</option>
                        <option value="30">Science: Gadgets</option>
                        <option value="31">Entertainment: Japanese Anime & Manga</option>
                        <option value="32">Entertainment: Cartoon & Animations</option>
                    </Input>
                </FormGroup>
                <FormGroup>
                    <Label for="startFormSelectDifficulty">Select Difficulty:</Label>
                    <Input
                        type="select"
                        name="selectDifficulty"
                        id="startFormSelectDifficulty"
                        value={this.state.difficulty}
                        onChange={this.chooseDifficulty}>
                            <option value="easy">Easy</option>
                            <option value="medium">Medium</option>
                            <option value="hard">Hard</option>
                    </Input>
                </FormGroup>
                <Button>Submit</Button>
            </Form>
        );
    };

}


export default StartForm;